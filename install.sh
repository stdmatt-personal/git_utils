#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : install.sh                                                    ##
##  Project   : git_utils                                                     ##
##  Date      : Dec 30, 2018                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2018 - 2020                                         ##
##                                                                            ##
##  Description :                                                             ##
##    Super, super simple install script for git_utils.                       ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
INSTALL_DIR="$HOME/.stdmatt/git_utils";
SCRIPT_DIR=$(pw_get_script_dir);
CREDENTIAL_CACHE_TIMEOUT=$(( 3600 * 24 )); ## 1hour * 24hours, a whole day!

##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
_install_source_on()
{
    local PATH_TO_INSTALL="$1";
    echo "Installing git_utils on ($PATH_TO_INSTALL)";

    if [ ! -e "$PATH_TO_INSTALL" ]; then
        touch "$PATH_TO_INSTALL";
    fi;

    local result=$(cat "$PATH_TO_INSTALL" | grep "source $INSTALL_DIR/git_utils.sh");
    if [ -z "$result" ]; then
        echo "## stdmatt's git_utils ##" >> "$PATH_TO_INSTALL";
        echo "source $INSTALL_DIR/git_utils.sh" >> "$PATH_TO_INSTALL";
    fi;
}

##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
echo "[Installing git_utils]";

##
## Install the script files.
##   Clear the installation directory.
rm    -rf "$INSTALL_DIR";
mkdir -p  "$INSTALL_DIR";

##
##   Copy all scripts to it.
cp -R $SCRIPT_DIR/src/* $INSTALL_DIR;

##
## Add a entry on the .bash_rc / .bash_profile so we can use the dots files.
DEFAULT_BASH_RC=$(pw_get_default_bashrc_or_profile);
USE_BASH_RC=$(pw_getopt_exists "$@" "--bashrc");
USE_BASH_PROFILE=$(pw_getopt_exists $@ "--bash-profile");

if [ -n "$USE_BASH_RC" ]; then
    _install_source_on "$HOME/.bashrc";
elif [ -n "$USE_BASH_PROFILE" ]; then
    _install_source_on "$HOME/.bash_profile";
else
    _install_source_on $DEFAULT_BASH_RC;
fi

##
## Set the author on the installation.
## 99% of the time I'm using my personal credentials anyways...
git config --global user.name  "stdmatt";
git config --global user.email "stdmatt@pixelwizards.io";

##
## Set the credential cache - So we don't need to keep entering the passwords.
git config --global credential.helper "cache --timeout=$CREDENTIAL_CACHE_TIMEOUT";

##
## On Windows we don't want to keep track of file modes
## since they are broken anyways.
if [ "$(pw_os_get_simple_name)" == "$(PW_OS_WINDOWS)" ]; then
    echo "Windows machine! Configuring the file mode to be ignored.";
    git config --global       core.filemode false;
fi;

##
## Merge by default...
git config --global pull.rebase false

echo "user.name     : $(git config --global user.name)";
echo "user.email    : $(git config --global user.email)";
echo "core.filemode : $(git config --global core.filemode)";
