##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : git_utils.sh                                                  ##
##  Project   : git_utils                                                     ##
##  Date      : Feb 25, 2017                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2017, 2018                                          ##
##                                                                            ##
##  Description :                                                             ##
##    Functions to save/export manual pages pdf format.                       ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## "Constants"                                                                ##
##----------------------------------------------------------------------------##
_GIT_UTILS_BRANCH_SEPARATOR_CHAR="-";


##----------------------------------------------------------------------------##
## Version                                                                    ##
##----------------------------------------------------------------------------##
GIT_UTILS_VERSION="1.0.0";
git_utils_version()
{
    echo "$GIT_UTILS_VERSION";
}


##----------------------------------------------------------------------------##
## Helper  Functions                                                          ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
_analytics_record()
{
    local uname_result=$(uname -a);
    local FUNC_NAME="${FUNCNAME[1]}";

    echo "$uname_result : $PWD : $(date -u) : ${FUNC_NAME} $@" >> ~/git_utils_analytics.txt;
}

##------------------------------------------------------------------------------
_git_utils_dialog_options()
{
    echo $(ichoices $@)
}

##------------------------------------------------------------------------------
_git_utils_concat_branch_name()
{
    local ARGS="$@";
    local BRANCH_NAME="$(pw_string_replace "$ARGS" " " "$_GIT_UTILS_BRANCH_SEPARATOR_CHAR")";
    echo "$BRANCH_NAME";
}


##----------------------------------------------------------------------------##
## Authors                                                                    ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
## @brief List all the authors sorted by commit count.
gauthors-top()
{
    git shortlog -s -n;
    _analytics_record;
}

##------------------------------------------------------------------------------
## @brief Show my credentials in the current repository.
gwhoami()
{
    local USER_NAME="$(git config user.name)";
    local USER_EMAIL=$(git config user.email);
    echo "$USER_NAME <$USER_EMAIL>"

    _analytics_record "$USER_NAME $USER_EMAIL";
}

##------------------------------------------------------------------------------
## @brief Set my credentials for the current repository.
## @param $1 author (optional) - The name of the commiter.
## @param $2 email  (optional) - The email of the commiter.
## @notes If the author / email is not set it will be defaulted to
##        stdmatt <stdmatt@pixelwizards.io>
gauthor-set()
{
    local ARGS="$@";
    local GLOBAL="";

    ## Check if we are setting globally.
    if [ $(pw_getopt_exists "$ARGS" "--global") ]; then
        GLOBAL="--global";
        ARGS=$(pw_string_replace "$ARGS" "--global" "");
    else
        GLOBAL=""; ## make sure to ignore everything else...
    fi;

    ## We need to create an array to actually get the values
    ## Since we can't set to $@.
    pw_array_create MY_ARGS "$ARGS";

    local AUTHOR="${MY_ARGS[0]}";
    local EMAIL="${MY_ARGS[1]}";

    unset MY_ARGS; ## Don't forget to delete the array the we created.


    if [ -z "$AUTHOR" ]; then
        AUTHOR="stdmatt";
    fi;

    if [ -z "$EMAIL" ]; then
        EMAIL="stdmatt@pixelwizards.io";
    fi;

    echo "- Setting git author - ";
    echo "Author : $AUTHOR";
    echo "Email  : $EMAIL";
    echo "Global : $GLOBAL";

    ## @XXX(stdmatt): By some reason on gitbash in Windows
    ## the empty expansion of $GLOBAL leads to an error.
    ##
    ## This is what the git outputs...
    ##    error: key does not contain a section:
    ##    error: key does not contain a section:
    ##
    ## This is a hack to make the thing works for now...
    if [ -z "$GLOBAL" ]; then
        git config user.name  "$AUTHOR";
        git config user.email "$EMAIL";
    else
        git config --global user.name  "$AUTHOR";
        git config --global user.email "$EMAIL";
    fi;

    _analytics_record "$AUTHOR $EMAIL $GLOBAL";
}


##----------------------------------------------------------------------------##
## Branch                                                                     ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
gcheckout()
{
    local BRANCHES=$(git branch --all | grep -v HEAD | grep -v "*");

    local TARGET_BRANCH="$(_git_utils_dialog_options $BRANCHES)";
    test -z "$TARGET_BRANCH"                         \
        && echo "[gcheckout] Not branch selected..." \
        && return 1;

    local CLEAN_BRANCH_NAME="$(echo $TARGET_BRANCH | sed s%remotes/origin/%%g)";
    echo "Checking out to branch [$CLEAN_BRANCH_NAME]";

    git checkout "$CLEAN_BRANCH_NAME";
    _analytics_record "$CLEAN_BRANCH_NAME";
}

##------------------------------------------------------------------------------
## @brief List all branches.
gbranch()
{
    git branch;
    _analytics_record;
}

##------------------------------------------------------------------------------
## @brief List all branches.
gbranch-all()
{
    git branch --all;
    _analytics_record;
}

##------------------------------------------------------------------------------
## @brief List the current branch.
gbranch-curr()
{
    git rev-parse --abbrev-ref HEAD;
    _analytics_record;
}

##------------------------------------------------------------------------------
## @brief Creates a branch with the given name.
## @param $1 name - The name of the branch.
gbranch-create()
{
    local NAME=$(_git_utils_concat_branch_name "$@");
    echo "[gbranch-create] Creating branch: ($NAME)";

    test -z "$NAME" &&                                            \
        echo "[gbranch-create] Branch name can't be empty..."  && \
        return 1;

    git checkout -b "$NAME";
    _analytics_record "$NAME";
}

##------------------------------------------------------------------------------
## @brief Creates a branch with the given suffix.
## @param $1 suffix - The suffix of the branch that will be created.
## @example Let the current branch be called "master", calling this function
##          with the argument of "test" will create a branch called "master-test".
gbranch-create-with-suffix()
{
    local SUFFIX=$(_git_utils_concat_branch_name "$@");
    test -z "$SUFFIX" && echo "Suffix can't be empty..." && return 1;

    local CURR_BRANCH_NAME="$(gbranch-curr)";
    local FINAL_BRANCH_NAME=$(_git_utils_concat_branch_name "$CURR_BRANCH_NAME" "$SUFFIX");
    echo "[gbranch-create-with-suffix] Creating branch: ($FINAL_BRANCH_NAME)";

    git checkout -b "$FINAL_BRANCH_NAME";
    _analytics_record "$SUFFIX";
}

##------------------------------------------------------------------------------
## @brief Delete the branch.
## @param $1 target_branch - The name of the branch that will be deleted.
gbranch-delete()
{
    ## todo(stdmatt): Add way to get the command line arguments and
    ##    get if user passed a --remote flag, meaning that we want to
    ##    remove the remote branch as well.
    local TARGET_BRANCH="$1";

    ## Empty branch...
    test -z "$TARGET_BRANCH" && \
        echo "Empty branch name - Ignoring..." && \
        return 0;

    ## Same branch...
    test "$(gbranch-curr)" == "$TARGET_BRANCH" && \
        echo "Trying to delete the current branch - Ignoring..." && \
        return 0;

    ## Invalid branch...
    local RESULT=$(git rev-parse --verify "$TARGET_BRANCH" 2> /dev/null);
    test -z "$RESULT" && \
        echo "Invalid branch name - Ignoring..." && \
        return 0;

    ## Here we have a valid branch that we can delete...
    ##   Passing '-d' instead of '-D' prevents us to making silly things.
    git branch -d "$TARGET_BRANCH";

    _analytics_record "$TARGET_BRANCH";
}

##------------------------------------------------------------------------------
gbranch-delete-merged-in-origin()
{
    ## @todo(stdmatt): Add way to delete the branches locally as well...
    git branch -r --merged  \
       | grep -v master     \
       | sed 's/origin\///' \
       | xargs -n 1 git push --delete origin

    _analytics_record;
}


##----------------------------------------------------------------------------##
## "Flow"                                                                     ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
gfeature()
{
    local BRANCH_NAME=$(_git_utils_concat_branch_name "$@");

    test -z "$BRANCH_NAME"                                         \
        && echo "[gfeature] Feature branch name can't be empty..." \
        && return 1;

    local FINAL_BRANCH_NAME="feature/$BRANCH_NAME";
    echo "[gfeature] Creating branch: ($FINAL_BRANCH_NAME)";

    git checkout -b "$FINAL_BRANCH_NAME";
    _analytics_record "$FINAL_BRANCH_NAME";
}

##------------------------------------------------------------------------------
gbugfix()
{
    local BRANCH_NAME=$(_git_utils_concat_branch_name "$@");

    test -z "$BRANCH_NAME"                                        \
        && echo "[gbugfix] Feature branch name can't be empty..." \
        && return 1;

    local FINAL_BRANCH_NAME="feature/$BRANCH_NAME";
    echo "[gbugfix] Creating branch: ($FINAL_BRANCH_NAME)";

    git checkout -b "$FINAL_BRANCH_NAME";
    _analytics_record "$FINAL_BRANCH_NAME";
}

##------------------------------------------------------------------------------
gmerge-i()
{
    local BRANCHES="$(gbranch | grep -v "*")";

    local TARGET_BRANCH_NAME="$(_git_utils_dialog_options $BRANCHES)";
    test -z "$TARGET_BRANCH_NAME" && echo "[gmerge-i] No selected branch" && return 0;

    echo "[gmerge-i] Merge to branch: $TARGET_BRANCH_NAME"
    echo "confirm ?";
    read

    if [ "$REPLY" == "y" ]; then
        gmerge-to "$TARGET_BRANCH_NAME" "$@";
        _analytics_record "[confirmed] $TARGET_BRANCH_NAME";
    else
        echo "Aborting...";
        _analytics_record "[abort] $REPLY $TARGET_BRANCH_NAME";
    fi;
}

##------------------------------------------------------------------------------
gmerge-from()
{
    local ARGS_STR="$@";
    local CURR_BRANCH="$(gbranch-curr)";
    local TARGET_BRANCH="$1";
    local SHOULD_PULL="false";
    local FORCE_MERGE="false";

    if [ -z "$TARGET_BRANCH" ]; then
        echo "[gmerge-from] - Target branch is empty. Aborting...";
        return 1;
    fi;

    if [ $(pw_getopt_exists "--pull" "$@") ]; then
        SHOULD_PULL="true";
        TARGET_BRANCH=$(pw_string_replace "$ARGS_STR" "--pull" "");
        TARGET_BRANCH=$(pw_trim "$TARGET_BRANCH");
    fi;

    if [ $(pw_getopt_exists "--force" "$@") ]; then
        FORCE_MERGE="true";
        TARGET_BRANCH=$(pw_string_replace "$ARGS_STR" "--force" "");
        TARGET_BRANCH=$(pw_trim "$TARGET_BRANCH");
    fi;

    if [ "$FORCE_MERGE" == "false" ]; then
        local STATUS=$(git status -s);
        test -n "$STATUS" && echo "Repository is not clean - Aborting..." && return 1;
    fi;

    echo "ARGS_STR              ($ARGS_STR)";
    echo "TARGET_BRANCH         ($TARGET_BRANCH)";
    echo "CURR_BRANCH           ($CURR_BRANCH)"
    echo "FORCE_MERGE           ($FORCE_MERGE)";
    echo "SHOULD_PULL           ($SHOULD_PULL)";

    git checkout "$TARGET_BRANCH";
    gpull-curr

    if [ "$?" != "0" ]; then
        echo "[gmerge-from] An error happened when tried to pull ($TARGET_BRANCH).";
        return 1;
    fi;

    if [ "$FORCE_MERGE" == "true" ]; then
        gmerge-to "$CURR_BRANCH" "--force";
    else
        gmerge-to "$CURR_BRANCH";
    fi;

    _analytics_record;
}

##------------------------------------------------------------------------------
gmerge-to()
{
    local ARGS_STR="$@";
    local TARGET_BRANCH_NAME="$1";
    local FORCE_MERGE="false";
    local CURR_BRANCH_NAME=$(gbranch-curr);

    if [ $(pw_getopt_exists "--force" "$@") ]; then
        FORCE_MERGE="true";
        TARGET_BRANCH_NAME=$(pw_string_replace "$ARGS_STR" "--force" "");
        TARGET_BRANCH_NAME=$(pw_trim "$TARGET_BRANCH_NAME");
    fi;

    test -z "$TARGET_BRANCH_NAME" && echo "Target branch can't be empty..." && return 1;

    if [ "$FORCE_MERGE" == "false" ]; then
        local STATUS=$(git status -s);
        test -n "$STATUS" && echo "Repository is not clean - Aborting..." && return 1;
    fi;

    # echo "ARGS_STR              ($ARGS_STR)";
    # echo "TARGET_BRANCH_NAME    ($TARGET_BRANCH_NAME)";
    # echo "FORCE_MERGE           ($FORCE_MERGE)";
    # echo "CURR_BRANCH_NAME      ($CURR_BRANCH_NAME)";

    git checkout "$TARGET_BRANCH_NAME";
    git merge --no-ff "$CURR_BRANCH_NAME";

    _analytics_record "$TARGET_BRANCH_NAME $CURR_BRANCH_NAME";
}

##----------------------------------------------------------------------------##
## Commit                                                                     ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
gadd()
{
    git add $@
    _analytics_record "$@";
}

##------------------------------------------------------------------------------
## @brief Commit the changes.
gcommit()
{
    local msg="$@";
    if [ -n "$msg" ]; then
        echo "Committing with msg: ($msg)";
        git commit -m "$msg";
    else
        git commit;
    fi;

    _analytics_record "$msg";
}

gcommit-change-author()
{
    local COMMIT_SHA="$1";
    local AUTHOR="$2";
    test -z "$COMMIT_SHA" && echo "Empty sha" && return;
    test -z "$AUTHOR" && echo "Empty author" && return;

    git rebase                                             \
        --onto ${COMMIT_SHA}                               \
        --exec "git commit --amend --author=\"${AUTHOR}\"" \
        $COMMIT_SHA;
}

##------------------------------------------------------------------------------
gcommit-reset-last-author()
{
    ## @todo(stdmatt): Get the author name from command line.
    git commit --amend --author="$(gauthor-whoami)";
    _analytics_record;
}

##------------------------------------------------------------------------------
grevert-file()
{
    ## @todo(stdmatt): Make this works for more than on file, and handle errors.
    git checkout -- "$1";
    _analytics_record "$$1";
}

##----------------------------------------------------------------------------##
## Log                                                                        ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
gdiff()
{
    git diff $@
    _analytics_record;
}

##------------------------------------------------------------------------------
glog() ## Thanks to: https://stackoverflow.com/a/9074343
{
    git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all
    _analytics_record;
}


##----------------------------------------------------------------------------##
## Submodule                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
## @brief Initialize all the submodules.
gsub-uir()
{
    git submodule update --init --recursive;
    _analytics_record;
}

##------------------------------------------------------------------------------
## @brief Reset **EVERYTHING** in the submodules.
gsub-clean-all-the-motherfucker-things()
{
    git submodule update --init --recursive;
    git submodule foreach --recursive git clean -ffdx
    git submodule foreach --recursive git reset --hard

    _analytics_record;
}

##------------------------------------------------------------------------------
## @brief Add the given submodule.
gsub-add()
{
    git submodule add $@;
    _analytics_record "$@";
}


##----------------------------------------------------------------------------##
## @todo(stdmatt): Find better section title...                               ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
gsync()
{
    gpull-curr;
    local RET=$?;
    if [ $RET != 0 ]; then
        echo "[gsync] git pull was unsucessful - Aborting...";
        return 1;
    fi;

    gpush-curr;
    local RET=$?;
    if [ $RET != 0 ]; then
        echo "[gsync] git push was unsucessful - Aborting...";
        return 1;
    fi;

    _analytics_record;
}

##------------------------------------------------------------------------------
gpush-curr()
{
    local CURRENT_BRANCH="$(gbranch-curr)";
    git push --tags origin "$CURRENT_BRANCH";
    local RET=$?;

    _analytics_record "$CURRENT_BRANCH";
    return $RET;
}

##------------------------------------------------------------------------------
gpull-curr()
{
    local CURRENT_BRANCH="$(gbranch-curr)";
    git pull origin "$CURRENT_BRANCH";
    local RET=$?;

    _analytics_record "$CURRENT_BRANCH";
    return $RET;
}

##------------------------------------------------------------------------------
gfetch()
{
    git fetch --tags;
    _analytics_record;
}

##------------------------------------------------------------------------------
gstatus()
{
   git status;
   _analytics_record;
}


##----------------------------------------------------------------------------##
## Repository                                                                 ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
greset()
{
    git reset --hard;
    _analytics_record;
}

##------------------------------------------------------------------------------
grepo-root()
{
    git rev-parse --show-toplevel
    _analytics_record;
}

##------------------------------------------------------------------------------
grepo-create()
{
    local DIRECTORY="$1";

    if [ -z "$DIRECTORY" ]; then
        echo "[grepo-create] Directory can't be empty.";
        _analytics_record "[empty directory]";

        return 1;
    fi;

    if [ -e "$DIRECTORY" ]; then
        echo "[grepo-create] Directory already exists.";
        _analytics_record "[directory exists] $DIRECTORY";

        return 1;
    fi;

    _analytics_record "[created] $DIRECTORY";

    mkdir -vp "$1" && cd "$DIRECTORY" && git init && pwd;
}

##------------------------------------------------------------------------------
## @brief Reset the repository to the cleanest state possible.
grepo-clean-all-motherfucker-things()
{
    local msg="$(pw_BR $(pw_FC RESETING ALL THE REPOSITORY?))";

    ## Just to show that we might making shit...
    echo "$msg";
    echo "$msg";
    echo "$msg";
    local reply=$(pw_prompt "$msg" "Yy");

    if [ "$reply" == "$PW_PROMPT_REPLY_YES" ]; then
        git reset --hard;
        gsub-clean-all-the-motherfucker-things
    fi;

    _analytics_record;
}

##------------------------------------------------------------------------------
grepo-url()
{
    git remote -v | head -1 | expand -t1 | cut -d" " -f2;

    _analytics_record;
}

##------------------------------------------------------------------------------
grepo-add-origin()
{
    local URL="$1";
    if [ -z "$URL" ]; then
        echo "[grepo-add-origin] URL can't be empty.";

        _analytics_record "[empty url]";
        return 1;
    fi;

    git remote add origin "$URL";

    _analytics_record "$URL";
}

##------------------------------------------------------------------------------
grepo-set-origin()
{
    local URL="$1";
    if [ -z "$URL" ]; then
        echo "[grepo-set-origin] URL can't be empty.";

        _analytics_record "[empty url]";
        return 1;
    fi;


    git remote set-url origin "$URL";

    _analytics_record "$URL";
}


##----------------------------------------------------------------------------##
## MISC                                                                       ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
gg()
{
    local GIT_GUI="git gui ";

    local SUBMODULES="$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }')";
    for SUBMODULE in $SUBMODULES; do
        pw_pushd "$SUBMODULE";
        local IS_DIRTY="$(git status -suno)";
        if [ -n "$IS_DIRTY" ]; then
            $GIT_GUI &
        fi;
        pw_popd;
    done;

    $GIT_GUI &
    _analytics_record;
}

##------------------------------------------------------------------------------
git-ignore-file-mode()
{
    git config core.filemode false;
}


##------------------------------------------------------------------------------
gignore()
{
    local ROOT_DIR=$(git rev-parse --show-toplevel 2> /dev/null);
    test -z "$ROOT_DIR" && echo "[gignore] not in a git repository." && return 1;

    local GITIGNORE_FILENAME="${ROOT_DIR}/.gitignore";
    test ! -f "$GITIGNORE_FILENAME" && touch "$GITIGNORE_FILENAME";

    local ARGS="$@";
    for ARG in $ARGS; do
        local CONTAINS=$(grep "$ARG" "$GITIGNORE_FILENAME");
        if [ -n "$CONTAINS" ]; then
            echo "[gignore] Already contains ($ARG) - Ignoring...";
        else
            echo "[gignore] Adding ($ARG)...";
            echo "$ARG" >> "$GITIGNORE_FILENAME";
        fi;
    done;

    _analytics_record;
}

##------------------------------------------------------------------------------
gignore-edit()
{
    local REPO_ROOT="$(git rev-parse --show-toplevel)";
    local IGNORE_FILE="${REPO_ROOT}/.gitignore";
    ## @todo(stdmatt): Fix the editor...
    $VISUAL "$IGNORE_FILE";

    _analytics_record;
}

##------------------------------------------------------------------------------
gignore-defaults-for()
{
    local BASE_URL="https://www.gitignore.io/api";
    local TARGET="$@";

    test -z "$TARGET"                                   \
        && "pw_func_log Missing argument - Aborting..." \
        && return 1;

    local FULL_URL="${BASE_URL}/${TARGET}";
    local TMP_FILE="/tmp/gignore.tmp";

    pw_func_log \
        "Fetching gitignore defaults from: (${FULL_URL}) for (${TARGET})";

    pw_network_simple_url_downloader "$FULL_URL" "$TMP_FILE" #> /dev/null 2>1;
    test $? != 0                                                                 \
        && pw_func_log "Failed to fetch gitignore for ($TARGET) at ($FULL_URL)." \
        && return 1;

    local REPO_ROOT="$(git rev-parse --show-toplevel)";
    local IGNORE_FILE="${REPO_ROOT}/.gitignore";
    cat "$TMP_FILE" >> "$IGNORE_FILE";

    echo "Done...";
    _analytics_record "$TARGET";
}

##------------------------------------------------------------------------------
git-creation-time-of()
{
    local FILENAME="$1";
    test -z "$FILENAME" && echo "[git-creation-time-of] URL can't be empty." && return 1;

    git log --follow --format=%aD --reverse -- $FILENAME | head -1

    _analytics_record;
}

##------------------------------------------------------------------------------
gwhat-i-did()
{
    local WHO_AM_I=$(gauthor-whoami);
    local SINCE="$1";

    git log --author="$WHO_AM_I"  \
            --since="$SINCE"      \
            --no-merges           \
            --oneline             \
            --format=format:'%s';

    _analytics_record;
}

##------------------------------------------------------------------------------
gwhat-i-did-yesterday()
{
    local SINCE="yesterday.0:00am";
    gwhat-i-did "$SINCE";

    _analytics_record;
}

##------------------------------------------------------------------------------
git-what-i-did-this-week()
{
    local SINCE="1 weeks ago";
    gwhat-i-did "$SINCE";

    _analytics_record;
}
